#!/bin/bash

# Running cppcheck on the source files
cppcheck --enable=all --addon=misra --suppressions-list=./ci/suppressions.txt --force --inline-suppr --error-exitcode=1 \
      -j 10 --std=c++17 --xml --xml-version=2 2>report.xml -I ./inc ./src -i ./src/Platform/ -i ./inc/Platform/
