# Business Logic Software

In this repository is where all of the ADCS on Board Software is housed. This is the final version that will be used on the satellite.
The software runs on the Microchip ATSAMV71Q21B Microcontroller Unit, which is a 32-bit ARM Cortex-M7 core.
We use FreeRTOS to handle the tasking of the MCU.

## Build
The `Business Logic Software` repository can be built without any access to specialised hardware. We provide build
instructions for Linux (or WSL) command-line, and the CLion IDE.

The dependencies of this repository are managed through the [conan](https://conan.io/) package manager, with repositories
from [ConanCenter](https://conan.io/center/) and [SpaceDot's packages](https://artifactory.spacedot.gr).

For more detailed installation instructions, including how to integrate with a microcontroller, visit the
[corresponding documentation page](https://acubesat.gitlab.io/obc/ecss-services/docs/md_docs_installation.html).

### From the Command Line (CLI)

1. Install a modern C++ compiler, CMake, and Conan.  
   CMake >= 3.23 and Conan >= 2.0 are recommended.
   <details>
   <summary>Getting conan</summary>

   You can install [conan](https://conan.io/) following the instructions [from here](https://docs.conan.io/2/installation.html).
   </details>
2. Clone the repository and enter the directory:
   ```shell
   git clone git@gitlab.com:acubesat/adcs/adcs-software/business-logic-software.git
   cd business-logic-software
   ```
3. (If you haven't already) create a conan profile for your system:
   ```shell
   conan profile detect
   ```
4. (If you haven't already) add the SpaceDot repository to conan:
   ```shell
   conan remote add spacedot https://artifactory.spacedot.gr/artifactory/api/conan/conan
   ```
5. Download all dependencies and build the project through conan:
   ```shell
   conan install . --output-folder=cmake-build-debug -u --build="*"  --settings=build_type=Debug -pr conan-profile
   ```
6. Download all submodules:
   ```shell
   conan source .
   ```
7. Add CMake flags:
   ```shell
   conan build . --output-folder=build --build=missing --update --settings=build_type=Debug
   ```
8. Build the project:
   ```shell
   cd build/Debug
   make
   ```
### From CLion

CLion will automatically try to set up a CMake project for you. However, without the conan packages installed, this
will quickly fail. Follow these steps to set up the conan project:

1. Follow steps 1-6 from the CLI instructions above.
2. Add the following to the CMake Options (File -> Settings -> Build, Execution, Deployment -> CMake -> CMake Options):
   ```
   --preset=cmake-build-debug-debug
   ```
   and in the build directory:
   ```shell
   cmake-build-debug/Debug
   ```

3. If your CMake project doesn't reload automatically, reload it manually (Tools -> CMake -> Reload CMake Project).

We do not recommend using a Conan plugin for your IDE, as it may tamper with the default configuration for this repository.


<details>
<summary>Getting conan</summary>

You can install [conan](https://conan.io/) following the instructions from
[here](https://docs.conan.io/2/installation.html).:
</details>

### Tests
To run the cppchecks locally, run:

```bash
./ci/cppcheck.sh
```

