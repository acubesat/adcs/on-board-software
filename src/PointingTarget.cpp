#include "PointingTarget.hpp"
// NOLINTBEGIN(readability-identifier-length)
using namespace Eigen;

PointingTarget::PointingTarget(const Matrix<float, 3, 3>& Kp, const Matrix<float, 3, 3>& Kd) :
        Kp{Kp},
        Kd{Kd} {}

void PointingTarget::setKp(const Matrix<float, 3, 3> &Kp) {
    this->Kp = Kp;
}

const Matrix<float, 3, 3>& PointingTarget::getKp() const {
    return Kp;
}

void PointingTarget::setKd(const Matrix<float, 3, 3>& Kd) {
    this->Kd = Kd;
}

const Matrix<float, 3, 3>& PointingTarget::getKd() const {
    return Kd;
}
// NOLINTEND(readability-identifier-length)